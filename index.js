// console.log("Hello World!");



	
/*	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function*/

		function checkAddTwoNumbers(num){
			let firstNumber = 5;
			let secondNumber =15;


			console.log("Displayed sum of " + firstNumber + " and " + secondNumber);
			console.log(firstNumber + secondNumber);
		}

		checkAddTwoNumbers();

		function checkSubTwoNumbers(num){
			let firstNumber = 20;
			let secondNumber =5;


			console.log("Displayed difference of " + firstNumber + " and " + secondNumber);
			console.log(firstNumber - secondNumber);
		}

		checkSubTwoNumbers();


		
		


	/*2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.*/


		function checkMultiTwoNumbers(num){
			let firstNumber = 50;
			let secondNumber =num;


			console.log("The product of " + firstNumber + " and " + num+":");
			console.log(firstNumber * secondNumber);
		}

		checkMultiTwoNumbers(10);

		function checkDivTwoNumbers(num){
			let firstNumber = 50;
			let secondNumber =num;


			console.log("The quotient of " + firstNumber + " and " + num+":");
			console.log(firstNumber / num);
		}

		checkDivTwoNumbers(10);




	/*3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.*/

		function circleArea(num) {
		    let area = Math.PI * (num * num);
		    
		    console.log("The result of getting the area of a circle with "+num+" radius:");
		    console.log(Math.round(area*100)/100);
		}
		 
		circleArea(15);


		 

/*	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	*/

	function averageVar(num) {
		    let num1 = 20;
		    let num2 = 40;
		    let num3 = 60;
		    let num4 = 80;
		    
		    console.log("The average of "+num1+","+num2+","+num3+","+num4+":");
		    console.log((num1+num2+num3+num4)/4);
		}
		 
		averageVar();

	/*5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.*/

		function isPassingScore(num){
			let grade = (num/50)*100;
			
			let isPassed = grade >= 75;
			console.log("Is " + num + "/50 a passing score?");
			console.log(isPassed);
		}

		isPassingScore(38);

		
